(function() {

    const submitBtn = document.querySelector('#lms-submit');
    const loginForm = document.getElementById('lms-login-form');
    const loader    = document.querySelector('.lms-form-loader');
    const username  = document.getElementById('lms-admin-username');
    const password  = document.getElementById('lms-admin-password');
    const message   = document.querySelector('.lms-field-message');
    
    loginForm.addEventListener('submit', function(event) {
        event.preventDefault();

        const baseUrl    = window.location.origin; 
        const submitData = async () => {

            // loading state 
            loader.classList.add('loading');

            const response = await fetch(`${baseUrl}/getCredentials`);
            const data     = await response.json();

            console.log(data);

            if(data[0].username === username.value && data[0].password === password.value) {

                loader.classList.remove('loading');

                window.localStorage.setItem('adminIsloggedIn', true);

                window.location = `${window.location.origin}/admin`;

            } else {

                message.classList.add('lms-message-show');

                loader.classList.remove('loading');

            }
           
        };

        submitData()
            .catch(error => {
                console.log(error);
            });
       
    });
   

})();