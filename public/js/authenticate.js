(function() {
    
    const app = {
        isLoggedIn: function() {

            const localStorage = window.localStorage;

            return localStorage.getItem('adminIsloggedIn') === null ? false : localStorage.getItem('adminIsloggedIn');

        },
        authenticate: function() {

            let app = this;
            let isLoggedIn = app.isLoggedIn();
            
            if(!isLoggedIn) {

                window.location = '/';

                return;

                
            }

            
            
        },
    
    }

    // initialze functions
    app.authenticate();
    

})();