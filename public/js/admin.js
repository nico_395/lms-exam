(function() {

    const app = {
        booksData: [],
        adminNavs: function() {

            // navigation tabs
            const adminNavs = document.querySelector('.lms-admin-navs');

            adminNavs.addEventListener('click', function(event) {
                

                if(event.target.classList.contains('lms-nav-btn')) {
                    let button = event.target;
                    let tabId = button.dataset.lmsTab;

                    // remove active class for each button
                    document.querySelectorAll('.lms-nav-btn').forEach(function(item) {
                        item.classList.remove('active');
                    });

                    // remove active class for each tab
                    document.querySelectorAll('.lms-tab').forEach(function(item) {
                        item.classList.remove('active');
                    });

                    // current active button
                    button.classList.add('active');

                    // current active tab
                    document.querySelector(`.lms-tab[data-lms-tab="${tabId}"]`).classList.add('active');
                }   
                
            });
        },
        getBooks: function() {
            const app      = this;
            const table    = document.querySelector('.lms-book-table');
            const loader   = document.querySelector('.lms-tab-books .lms-loader');
            const tableMessage = document.querySelector('.lms-books-table-message');

            const getData = async () => {

                // show loader
                table.classList.add('loading');

                const baseUrl  = window.location.origin; 
                const response = await fetch(`${baseUrl}/api/getBooks`);
                const responseData     = await response.json();

                // hide loader
                table.classList.remove('loading');

                if(responseData.length === 0) {
                    app.tableMessage('There\'s no books available..', '.lms-books-table-message');
                    tableMessage.classList.add('active');
                }

                // show message
                table.classList.add('loaded');

                // render books
                app.renderBooksHTML(responseData);

                 // add books global data
                 app.booksData = responseData;
            }


            getData()
                .catch((error) => {

                    // show message
                    table.classList.add('loaded');

                    app.tableMessage('There\'s a problem on fetching data.', '.lms-books-table-message');

                    tableMessage.classList.add('active');

                    console.log(error);
                })

        },
        addBook: function() {

            const addBookForm = document.getElementById('lms-add-book-form');

            addBookForm.addEventListener('submit', function(event) {
                event.preventDefault();

                const title = document.getElementById('lms-add-book-title').value;
                const author = document.getElementById('lms-add-book-author').value;
                const category = document.getElementById('lms-add-book-category').value;
                const isbn = document.getElementById('lms-add-book-isbn').value;
                const quantity = document.getElementById('lms-add-book-quantity').value;

                const bookObject = {
                    title,
                    author,
                    category,
                    isbn,
                    quantity
                }


                const sendData = async () => {
                    const baseUrl = window.location.origin;
                    const config  = {
                        method: 'POST',
                        headers: { 'Content-Type': 'application/json' },
                        body: JSON.stringify(bookObject)
                    };
                    const response = await fetch(`${baseUrl}/api/addBook`, config);
                    const responseData = await response.json();

                    // refresh book data
                    app.getBooks();

                    // reset form
                    addBookForm.reset();
                }

                sendData()
                    .catch(error => {
                        console.log(error);
                    });
                
            });
        },
        deleteBook: function() {
            const app      = this;
            const table    = document.querySelector('.lms-book-table');

            table.addEventListener('click', function(event) {
                const target = event.target;

                if(!target.classList.contains('lms-table-delete')) return;

                const loader       = document.querySelector('.lms-tab-books .lms-loader');
                const tableMessage = document.querySelector('.lms-books-table-message');
                const bookId       = target.parentElement.parentElement.dataset.bookId;
                const baseUrl      = window.location.origin;

                const deleteData   = async () => {
                    const response     = await fetch(`${baseUrl}/api/deleteBook/${bookId}`, {
                        method: 'DELETE',
                        headers: {
                            'Content-Type' : 'application/json'
                        }
                    });
                    const responseData = await response.json();
                    
                    // render books
                    app.getBooks();
                }

                deleteData()
                    .catch(error => {
                        console.log(error);
                    });

                console.log(`${baseUrl}/api/deleteBook/${bookId}`);
                

            });

        },
        updateBook: function() {

            const app        = this;
            const booksTable = document.querySelector('.lms-tab-books');
            const booksEditForm = document.getElementById('lms-edit-book-form');
            let title = document.getElementById('lms-edit-book-title');
            let author = document.getElementById('lms-edit-book-author');
            let category = document.getElementById('lms-edit-book-category');
            let isbn = document.getElementById('lms-edit-book-isbn');
            let quantity = document.getElementById('lms-edit-book-quantity');
            let currentBookId; 

            booksTable.addEventListener('click', function(event){
                const target = event.target;

                if(!target.classList.contains('lms-table-edit')) return;

                const booksData = app.booksData;
                currentBookId = target.parentElement.parentElement.dataset.bookId;
                let currentBook = booksData.find(book => book.book_id == currentBookId);
                
                title.value = currentBook.title;
                author.value = currentBook.author;
                category.value = currentBook.category;
                isbn.value = currentBook.isbn;
                quantity.value = currentBook.quantity;

            });

            booksEditForm.addEventListener('submit', function(event) {
                event.preventDefault();

                const updatedBookObject = {
                    book_id: currentBookId,
                    title: title.value,
                    author: author.value,
                    category: category.value,
                    isbn: isbn.value,
                    quantity: quantity.value
                };
                
                console.log(updatedBookObject);

                // submit updated book
                const submitData = async () => {
                    const baseUrl = window.location.origin;
                    const config  = {
                        method: 'PUT',
                        headers: { 'Content-Type': 'application/json' },
                        body: JSON.stringify(updatedBookObject)
                    };
                    const response = await fetch(`${baseUrl}/api/updateBook/`, config);
                    const responseData = await response.json();

                    console.log(responseData);

                    // refresh book data
                    app.getBooks();

                    // reset form
                    booksEditForm.reset();
                }

                submitData()
                    .catch(error => {

                        console.log(error);

                    })

            });

            

        },
        searchBook: function() {

            const app = this;
            const bookSearchForm = document.getElementById('lms-book-search');
            const tableMessage = document.querySelector('.lms-books-table-message');

            bookSearchForm.addEventListener('submit', function(event) {
                event.preventDefault();

                const field = document.getElementById('lms-search-book');
                const booksData = app.booksData;
                let filteredBooks;

                filteredBooks = booksData.filter(function(book) {
                    return Object.keys(book).some(function(prop) {
                        return book[prop].toString().toLowerCase().trim().includes(field.value.toLowerCase().trim());
                    });
                });

                if(filteredBooks.length) {

                    tableMessage.classList.remove('active');
                    app.renderBooksHTML(filteredBooks);

                } else {

                    tableMessage.classList.add('active');
                    document.querySelector('.lms-book-table table tbody').innerHTML = '';
                    app.tableMessage('No Results ...', '.lms-books-table-message');

                }

            });

        },
        renderBooksHTML: function(array) {

            let booksHTML = '';
            let container = document.querySelector('.lms-book-table tbody');

            array.forEach(book => {

                booksHTML += `<tr data-book-id="${book.book_id}">
                                <th scope="row">${book.book_id}</th>
                                <td>${book.title}</td>
                                <td>${book.author}</td>
                                <td>${book.category}</td>
                                <td>${book.isbn}</td>
                                <td>${book.quantity}</td>
                                <td>
                                    <button class="lms-table-action lms-table-edit lms-modal-trigger" data-modal="lms-edit-book-modal" aria-label="Edit Book">
                                        <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                                    </button>
                                    <button class="lms-table-action lms-table-delete" aria-label="Delete Book">
                                        <i class="fa fa-trash-o" aria-hidden="true"></i>
                                    </button>
                                </td>
                            </tr>`;
            });

            container.innerHTML = booksHTML;

        },
        tableMessage: function(message, selector) {

            document.querySelector(selector).innerHTML = message;

        },
        showModal: function() {

            document.body.addEventListener('click', function(event) {
                const target = event.target;
                

                if(!target.classList.contains('lms-modal-trigger')) return;
                
                const modalClass = target.dataset.modal;

                document.querySelector(`.${modalClass}`).classList.add('active');


            });
        },
        closeModal: function() {

            const modals = document.querySelectorAll('.lms-modal');

            document.body.addEventListener('click', function(event) {
                const target = event.target;
                

                if(!target.classList.contains('lms-close-modal')) return;
                console.log('clicked ;asdasd');
                modals.forEach(function(modal) {  modal.classList.remove('active') });

            });

        }
        

        
    };

    // initialize admin functions
    app.adminNavs();

    app.getBooks();

    app.addBook();

    app.deleteBook();

    app.showModal();

    app.closeModal();
    
    app.updateBook();

    app.searchBook();

})();