const express = require('express');
const app     = express();
const PORT   = 3000;
const routerBooks = require('./routes/books-routes');
const routerLogin = require('./routes/login-routes');

app.use(express.json());
app.use('/public', express.static('public'));
app.use('/api', routerBooks);
app.use('/', routerLogin);


// login page
app.get('/', (req, res) => {
    console.log(req.url)
    res.sendFile(__dirname + '/index.html');
});

// admin page
app.get('/admin', (req, res) => {
    console.log(req.url)
    res.sendFile(__dirname + '/views/admin.html');
});

// 404 page
app.get('*', (req, res) => {
    console.log(req.url)
    res.sendFile(__dirname + '/views/404.html');
});

app.listen(PORT, () => {
    console.log(`Listening at port: ${PORT}`);
});