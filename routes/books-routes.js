const express   = require('express');
const router    = express.Router();
const booksService = require('../service/books-service');

// get all books
router.get('/getBooks', async(request, response) => {

    try {
        const db     = booksService.getInstance();
        const result = await db.getAllBooks();

        response.json(result);

    } catch(err) {
        console.log(err);
    }

});

// add book
router.post('/addBook', async (request, response) => {
        const data = request.body; 


        try {
            const db     = booksService.getInstance();
            const result = await db.addBook(data);
            console.log(data);
            response.json(result);
            
        } catch(err) {

            console.log(err);

        }

});

// delete book
router.delete('/deleteBook/:id', async (request, response) => {
        const data = request.params;

        try {
        
            const db     = booksService.getInstance();
            const result = await db.deleteBook(data.id);
            console.log(result);
            response.json(result);

        } catch(error) {

            console.log(error);

        }

    });

// update book
router.put('/updateBook/', async (request, response) => {
    const data = request.body;
    console.log(data);

    try {
        const db     = booksService.getInstance();
        const result = await db.updateBook(data);
        
        response.json(result);

    } catch(error) {
        
        console.log(error);
        
    }

});

module.exports = router;