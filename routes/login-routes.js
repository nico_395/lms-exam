const express   = require('express');
const router    = express.Router();
const loginService = require('../service/login-service');

// get credentials
router.get('/getCredentials', async(request, response) => {

    try {
        const db     = loginService.getInstance();
        const result = await db.getCredentials();

        response.json(result);

    } catch(err) {
        console.log(err);
    }

});

module.exports = router;