const mysql = require('mysql');
let dbInstance = null;

const connectionConfig = {
    host: 'localhost',
    user: 'root',
    password: '',
    database: 'lms_db'
};

const connection = mysql.createConnection(connectionConfig);

// connect datebase
connection.connect((err) => {
    if(err) {
        console.log(err);
    }

    console.log(`db state: ${connection.state}`);
});

module.exports = connection;