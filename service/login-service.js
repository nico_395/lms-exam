let dbInstance  = null;
const connection = require('../db-config');

class LoginService {

    static getInstance () {
        return dbInstance ? dbInstance : new LoginService();
    }

    async getCredentials() {

        try {

            const response = await new Promise((resolve, reject) => {
                const sql = 'SELECT * FROM admin';

                connection.query(sql, (err, results) => {

                    if(err) reject(new Error(err.message));

                    resolve(results);

                })
            });

            return response;

        } catch (error) {

            console.log(error);

        }

    }


}

module.exports = LoginService;