let dbInstance  = null;
const connection = require('../db-config');

class BooksService {

    static getInstance () {
        return dbInstance ? dbInstance : new BooksService();
    }

    async getAllBooks() {

        try {

            const response = await new Promise((resolve, reject) => {
                const sql = 'SELECT * FROM books';

                connection.query(sql, (err, results) => {

                    if(err) reject(new Error(err.message));

                    resolve(results);

                })
            });

            return response;

        } catch (error) {

            console.log(error);

        }

    }

    async addBook(book) {

        try {
            
            const response = await new Promise((resolve, reject) => {
                const sql = `INSERT INTO books (title, author, category, isbn, quantity) VALUES (?, ?, ?, ?, ?)`;
               
                console.log(sql);
                
                connection.query(sql, [book.title, book.author, book.category, book.isbn, book.quantity], (err, results) => {
                    if(err) reject(new Error(err.message));
                    resolve(results);
                });
            });

            return response;

        } catch(err) {
            console.log(err);
        }   

    }
    

    async deleteBook(id) {

        try {

            const response = await new Promise((resolve, reject) => {
                console.log('test db');
                const sql = `DELETE FROM books WHERE book_id = ?`;

                connection.query(sql, [id], (error, results) => {

                    if(error) reject(new Error(error.message));

                    resolve(results);
                })

            });

            return response;

        } catch(error) {

            console.log(error);

        }
        
    }

    async updateBook(book) {

        try {
            const response = await new Promise((resolve, reject) => {
                
                const sql = `UPDATE books SET title = ?, author = ?, category = ?, isbn = ?, quantity = ? WHERE book_id = ?`;

                console.log(sql);

                connection.query(sql, [book.title, book.author, book.category, book.isbn, book.quantity, book.book_id], (error, results) => {
                    if(error) reject(new Error(error.message));
                    console.log(results);
                    resolve(results);
                });

            });

            return response;

        } catch(err) {

            console.log(err);

        }
    }

}

module.exports = BooksService;